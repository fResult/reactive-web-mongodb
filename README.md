# Spring Reactive Mongodb + Docker
This repository is created for playing around Spring Boot Webflux integrating with Mongodb + Docker.

I also write a blog post for this repository on Medium.\
Checkout this blog post here → https://medium.com/p/b742382182f2

## Prerequisite
- Your favorite IDE (I prefer the [IntelliJ](https://jetbrains.com/idea/download))
- [Docker](https://www.docker.com/get-started) (and Docker Compose)
- [Java (JDK) version 17 or more](https://www.oracle.com/java/technologies/jdk-script-friendly-urls) on your local machine (I use Java 21)
- [Gradle 8+](https://gradle.org/install)

## Script
Prepare Containerized Mongodb
```shell
docker-compose -f docker/compose.dev.yml up
```

Install dependencies.
```shell
./gradlew build
```

Run application
```shell
./gradlew bootRun
```

*If you wanna clear Containerized Mongodb for this project*
```shell
docker-compose -f docker/compose.dev.yml down
```
