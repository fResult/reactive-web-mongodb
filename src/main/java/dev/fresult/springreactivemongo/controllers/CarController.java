package dev.fresult.springreactivemongo.controllers;

import dev.fresult.springreactivemongo.documents.Car;
import dev.fresult.springreactivemongo.services.CarService;
import io.netty.handler.codec.DecoderException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

@RestController
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarController {
  private final CarService service;

  @GetMapping
  public Flux<Car> all(@RequestParam(required = false) String brand) {
    if (brand == null) return service.all();
    return service.byBrand(brand);
  }

  @GetMapping("/{id}")
  public Mono<ResponseEntity<?>> byId(@PathVariable String id) {
    return service.createCarMonoOpt(service.byId(id)).flatMap(flatMapMonoCarOK);
  }

  @PostMapping
  public Mono<ResponseEntity<?>> create(@RequestBody Car car, UriComponentsBuilder uriBuilder) {
    try {
      return service.create(car).flatMap(createdCar -> {
        String location = uriBuilder.path("cars/{id}").buildAndExpand(createdCar.getId()).toUriString();
        return Mono.just(ResponseEntity.created(URI.create(location)).body(createdCar));
      });
    } catch (DecoderException e) {
      System.err.println("error: " + e.getMessage());
      return Mono.just(ResponseEntity.badRequest().body(e.getCause()));
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return Mono.just(ResponseEntity.internalServerError().body(e.getCause()));
    }
  }

  @PutMapping("/{id}")
  public Mono<ResponseEntity<?>> update(@PathVariable String id, @RequestBody Car car) {
    try {
      return service.createCarMonoOpt(service.update(id, car))
          .flatMap(flatMapMonoCarOK);
    } catch (DecoderException e) {
      System.err.println("error: " + e.getMessage());
      return Mono.just(ResponseEntity.badRequest().body(e.getCause()));
    } catch (Exception e) {
      System.err.println(e.getMessage());
      return Mono.just(ResponseEntity.internalServerError().body(e.getCause()));
    }
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteById(@PathVariable String id) {
    service.deleteById(id).subscribe();
    return ResponseEntity.noContent().build();
  }

  private final Function<Optional<Car>, Mono<ResponseEntity<?>>> flatMapMonoCarOK = carOpt ->
      carOpt.<Mono<ResponseEntity<?>>>map(car -> Mono.just(ResponseEntity.ok(car)))
          .orElseGet(() -> Mono.just(ResponseEntity.notFound().build()));
}
