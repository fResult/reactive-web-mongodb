package dev.fresult.springreactivemongo;

import lombok.val;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringReactiveMongoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext app = SpringApplication.run(SpringReactiveMongoApplication.class, args);

		val env = app.getEnvironment();
		System.out.println(env.getProperty("spring.data.mongodb.uri"));
	}

}
