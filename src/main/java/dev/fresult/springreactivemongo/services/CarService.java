package dev.fresult.springreactivemongo.services;

import dev.fresult.springreactivemongo.documents.Car;
import dev.fresult.springreactivemongo.repositories.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarService {
  private final CarRepository repo;

  public Flux<Car> all() {
    return repo.findAll();
  }

  public Mono<Car> byId(String id) {
    return repo.findById(id);
  }

  public Flux<Car> byBrand(String brand) {
    return repo.findByBrandIgnoreCase(brand);
  }

  public Mono<Car> create(Car car) {
    return repo.save(car);
  }

  public Mono<Car> update(String id, Car car) {
    return createCarMonoOpt(repo.findById(id))
        .flatMap(carOpt -> {
          if (carOpt.isEmpty()) return Mono.empty();

          car.setId(id);
          return repo.save(car);
        });
  }

  public Mono<Void> deleteById(String id) {
    return repo.deleteById(id);
  }

  public Mono<Optional<Car>> createCarMonoOpt(Mono<Car> carMono) {
    return carMono.map(Optional::of).defaultIfEmpty(Optional.empty());
  }
}
