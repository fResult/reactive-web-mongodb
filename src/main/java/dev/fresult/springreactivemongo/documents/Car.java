package dev.fresult.springreactivemongo.documents;

import com.mongodb.lang.NonNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document
@RequiredArgsConstructor
public class Car {
  private @Id String id = null;
  private @NonNull String brand;
  private @NonNull String model;
}
